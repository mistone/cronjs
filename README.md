# cronjs
基于javascript实现crontab格式的定时任务,支持秒，分，时，天，月，年，周 设置

## 文档说明


```js
//cronjs支持的cron表达式
second minute hour day month week
```
- second： 表示秒，可以是从0到59之间的任何整数
- minute： 表示分钟，可以是从0到59之间的任何整数
- hour：表示小时，可以是从0到23之间的任何整数
- day：表示日期，可以是从1到31之间的任何整数
- month：表示月份，可以是从1到12之间的任何整数
- week：表示星期几，可以是从0到6之间的任何整数，这里的0代表星期日

详细crontab表达式可以参考网上crontab资料

## 使用方法
```js
//引入js
<script type="text/javascript" src="cron.js"></script>
//调用setCronTak(callback,cronstr)方法
setCronTak(callback, cronstr);
```

- callback 回调函数
- cronstr cron表达式

## 快速上手


```html
<!DOCTYPE html>
<html>

	<head>
		<meta charset="utf-8" />
		<title></title>
		<script type="text/javascript" src="cron.js"></script>
	</head>

	<body>
		<script>
			function showTime() {
				var now = new Date();
				console.log("-----" + now.toLocaleString());
			}
			//每5秒钟执行showTime函数
			setCronTak(showTime, "*/5 * * * * * ?");
		</script>
	</body>

</html>
```